package t1s3;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author anton
 */
public class Matriz4X3 {

    public void Matriz() {
        Random ran = new Random();
        Scanner CD = new Scanner(System.in);
        int filas = 0;
        int columnas = 0;
        int cont = 1;
        System.out.println("Numero de filas? ");
        filas = CD.nextInt();
        System.out.println("Numero de columnas? ");
        columnas = CD.nextInt();
        int[][] num = new int[filas][columnas];//Numero de filas y columnas ingresadas
        for (int f = 0; f < num.length; f++) {
            System.out.print("_");
            for (int g = 0; g < num[f].length; g++) {
                int nume = ran.nextInt(20) + 1;
                num[f][g] = nume;
                System.out.print("[" + num[f][g] + "]");
                System.out.print("_");
            }
            System.out.println("_");
        }
        int conta = 0;
        for (int j = 0; j < num.length; j++) {
            if (num[j][0] == 5) {
                conta++;
            }
        }
        System.out.println("Cantidad de filas que inician en 5 son: " + conta);
    }
}
