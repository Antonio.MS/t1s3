package t1s3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/*

 * @author anton
 */
public class DatosP {

    ArrayList<ObjetoPersona> Lista = new ArrayList<>();
    ObjetoPersona ob = new ObjetoPersona();

    public void Personas() {
        String borrar = "\u001B[0m";
        String azul = "\033[34m";
        String rojo = "\033[31m";

        Scanner sc = new Scanner(System.in);
        int j = 1;
        int f = 0;

        System.out.println("Cantidad de personas a ingresar: ");
        int canti = sc.nextInt();
        while (j <= canti) {
            sc = new Scanner(System.in);

            System.out.println("//---");
            System.out.println("Ingrese el nombre : ");
            ob.setNombre(sc.nextLine());
            System.out.println("Ingrese la edad : ");
            ob.setEdad(sc.nextInt());
            Lista.add(new ObjetoPersona(ob.getNombre(), ob.getEdad()));

            j++;
        }
        for (int i = 0; i < Lista.size(); i++) {
            if ((Lista.get(i).getEdad() % 2) == 0) {
                System.out.println("Nombre : " + Lista.get(i).getNombre() + " / Edad : " + azul + Lista.get(i).getEdad() + borrar);
            } else {
                System.out.println("Nombre : " + Lista.get(i).getNombre() + " / Edad : " + rojo + Lista.get(i).getEdad() + borrar);
            }

        }

    }
}
