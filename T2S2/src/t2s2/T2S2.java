/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package t2s2;

import java.util.Scanner;

/**
 *
 * @author anton
 */
public class T2S2 {

    public static void main(String[] args) {

        T2S2(args);
    }

    public static void T2S2(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i = 1;

        System.out.println("------------MENU--------------");
        System.out.println("SELECIONE LA OPCION A EJECUTAR");
        System.out.println("1.-SUMA NUMERO");
        System.out.println("2.-EDAD ACTUAL");
        System.out.println("3.-PROMEDIOS");
        System.out.println("4.-SALARIOS");
        System.out.println("5.-SALIR");
        int opcion = sc.nextInt();
        switch (opcion) {
            case 1://SumaNumero
                while (i <= 20) {//Mientras i sea menos que 20 entrara en el ciclo
                    System.out.println(i);
                    i = i + 1;//Esto hara que incremente hasta llegar a 20 

                }
                break;

            case 2://EdadCumplida
                Scanner cs = new Scanner(System.in);
                int valor1 = 0;
                int valor2 = 0;
                int edad = 0;
                int Cumple;
                System.out.println("Ingrese el año actual: ");
                valor1 = sc.nextInt();
                System.out.println("Ingrese el año en que nacio: ");
                valor2 = sc.nextInt();
                edad = valor1 - valor2;//Aca obtenemos la edad del usuario pero no sabemos si ya cumplio o esta por hacerlo
                System.out.println("¿Usted ya cumpio años?");
                System.out.println("1.-SI");//mantiene la resta con el año actual
                System.out.println("2.-NO");//de lo contrario le restamos un año porque aun no cumple
                Cumple = cs.nextInt();
                if (Cumple == 1) {
                    System.out.println("Su edad actual es: " + edad);

                } else {
                    if (Cumple == 2) {
                        edad = edad - 1;
                        System.out.println("Su edad actual es: " + edad);
                    }
                }
                break;

            case 3://Promedio
                Scanner numero = new Scanner(System.in);
                int j = 1;
                int prom = 0;
                int nota = 0;
                while (j <= 3) {//Condicion que permite el ingreso de 3 notas
                    System.out.println("Ingrese su nota: ");//Solicitamos su nota
                    nota = numero.nextInt();//Se guarda en nota
                    prom = prom + nota;//Sumamos todas las notas
                    j = j + 1;//Incremente de uno en uno el contador 

                }
                prom = prom / 3;//Ya sumadas todas las notas las dividimos entre 3 que nos dara el promedio
                System.out.println("Su promedio final es: " + prom);
                break;

            case 4://Salarios
                int HorasLab = 0;
                int PrecioHora = 0;
                int SalBruto = 0;
                int SalNeto = 0;
                int Deducc = 0;
                System.out.println("Ingrese cantidad de horas laboradas por mes: ");//Solicitamos canti de horas laboradas
                HorasLab = sc.nextInt();//guardamos
                System.out.println("Ingrese precio por cada hora laborada: ");//Solicitamos precio por hora
                PrecioHora = sc.nextInt();//guardamos
                SalBruto = HorasLab * PrecioHora;//Sacamos salario bruto cantidad de horas por precio de cada hora
                SalNeto = (int) (SalBruto - (SalBruto * (9.17 / 100)));//El salario neto le restamos el 9.17 de las cargas sociales
                Deducc = SalBruto - SalNeto;//Seria el resultado de de restarle el salrio neto al salario bruto
                System.out.println("Su salario Bruto es: " + SalBruto);
                System.out.println("Su salario Neto es: " + SalNeto);
                System.out.println("Deduccuones Totales: " + Deducc);

            case 5://Opcion salir
                break;
        }

    }

}
