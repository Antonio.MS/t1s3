/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numerosuerte;

import java.util.Scanner;

/**
 *
 * @author USER
 */
public class NumeroSuerte {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int dia, mes, año, suerte, suma, C1, C2, C3, C4;
        System.out.println("Introduzca fecha de nacimiento");
        System.out.print("Día: ");
        dia = sc.nextInt();
        System.out.print("Mes: ");
        mes = sc.nextInt();
        System.out.print("Año: ");
        año = sc.nextInt();
        suma = dia + mes + año;
        C1 = suma / 1000;      //obtiene la primera cifra
        C2 = suma / 100 % 10;    //obtiene la segunda cifra
        C3 = suma / 10 % 10;     //obtiene la tercera cifra
        C4 = suma % 10;        //obtiene la última cifra
        suerte = C1 + C2 + C3 + C4;
        System.out.println("Su número de la suerte es: " + suerte);
    }

}
